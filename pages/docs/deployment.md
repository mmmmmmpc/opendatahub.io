---
layout: docs
title: Deployment
permalink: /docs/deployment
---

Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.

# Requirements

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

* Lorem ipsum dolor sit amet
* Consectetur adipiscing elit
* Sed do eiusmod tempor
* Incididunt ut labore et dolore magna aliqua
* Duis aute irure dolor in reprehenderit
* sunt in culpa qui officia deserunt


```bash
# oc type stuff
$ oc get all
```

# Deploying from the Catalog

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat, [sample link](https://opendatahub.io). Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## Cupidatat Non Proident

1. Lorem ipsum dolor sit amet
1. Consectetur adipiscing elit
1. Sed do eiusmod tempor
1. Incididunt ut labore et dolore magna aliqua
1. Duis aute irure dolor in reprehenderit
1. sunt in culpa qui officia deserunt


# Configuration

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat, [sample link](https://opendatahub.io). Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

# Troubleshooting:

Lorem ipsum dolor sit amet [fugiat](https://opendatahub.io) ex ea commodo consequat [laborum](https://opendatahub.io).
